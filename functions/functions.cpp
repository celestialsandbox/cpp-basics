// functions.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "operations.h"


inline int moddiv(int f, int l)
{
	return l % 11;
}

typedef	struct Date
{
	int day;
	int month;
	int year;
};

int main()
{
	int w = 5;
	int&  z = w;

	w++;

	auto x = sum(3, 4);

	sum('x', 'y');

	sum(34.56, 56.7);

	//sum(34.67, 7);

	string result =  sum("hello", "world");

	cout << result.c_str() << endl;

	copied(x);

	cout << ADD(5, 6) << endl;

	cout << 5 + 6 << endl;

	int first=0, last = 60;
	cout << moddiv(first, last) << endl;

    return 0;
}

int	sum(int x, int y)
{
	return x + y;
}

char		sum(char x, char y)
{
	return x + y;
}

double		sum(double x, double y)
{
	return x + y;
}

string		sum(const string& x, const string& y)
{
	string temp = x + y;

	return temp;
}

void	copied(int& val)
{
	++val;
}

