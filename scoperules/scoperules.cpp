// scoperules.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

#include "course.h"

using namespace std;

long count; // total for all courses

void Course::reserve(int ct)
{   
	// sufficient places remaining?
	if (this->count + ct <= max_size())
	{
		count += ct; // update class count?        
	}
}

int main()
{
	Course c1;
	Course c2;

	c1.reserve(12);
	c2.reserve(14);

	c1 += 1;
	Course c3 = c1 + c2;
	return 0;
}

