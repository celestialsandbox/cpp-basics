#pragma once

#include <string>

using namespace std;

typedef struct Date
{
	short day;
	short month;
	short year;
};


class Person
{
public:
	Person() = default;
	Person(const string& name, const Date& dob)
		:name(name), dob(dob)
	{}

	~Person();

private:
	string name;
	Date dob;
};

