// Collections.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

class Test
{
public:
	Test(const string val ):data(val)
	{
		cout << data.c_str() << " being created" << endl;
	}
	~Test()
	{
		cout << data.c_str() << " being destroyed" << endl;
	}

	string& getData() { return data;  }
private:
	string	data;
};


template	<typename T>
class MyClass
{
public:
	MyClass() {}

	void	setData(T val) {
		data = val;
	}

	T getData() const
	{
		return data;
	}
	template <typename T>
	class iterator
	{
		void	method()
		{
			data++;
		}
	};

	iterator	begin()
	{
		return new iterator;
	}
private:
	T data;
};



int main()
{
	int pause;

	vector<Test*> vv = { new Test("Selvyn"), new Test("Wright") };
	
	vv[0]->getData();

	MyClass<int> mc;

	mc.setData(2);

	cin >> pause;

    return 0;
}

