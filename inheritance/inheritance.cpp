// inheritance.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Account
{
protected:
	string	accNo;
	double	balance;


	Account(const string& no, const double amt)
		:accNo(no), balance(amt)
	{
		cout << "Account::Account(no, amt)" << endl;
	}
public:
	~Account() {}

	double	debit(double amt)
	{
		return balance -= amt;
	}

	double	credit(double amt)
	{
		return balance += amt;
	}

	string&		getAccNo() { return accNo; }
	double		getBalance() { return balance; }
};

class CreditAccount: public Account
{
public:
	CreditAccount():Account("", 0.0)	// just for demo purposes
	{
		cout << "CreditAccount::CreditAccount()" << endl;
	}
	CreditAccount(const string& no, const double amt)
		:Account(no, amt)
	{
		cout << "CreditAccount::CreditAccount(no, amt)" << endl;
	}
	~CreditAccount() {}

	double	getODLimit() const
	{
		return odLimit;
	}
	double		getBalance() { return balance; }

private:
	double	odLimit = -200.00;
};

void	handleAccount(Account& cacc)
{
	CreditAccount& temp = static_cast<CreditAccount&>(cacc); // NO NO NO NO NO
	//++ remove comment below to see issue
	//cout << cacc.getAccNo().c_str() << "with OD of " << cacc.getODLimit() << endl;

	cacc.getBalance();
}

int main()
{
	Account acc("A/30456", 200.00);

	CreditAccount cacc("B/3565", 300.00);

	cacc.getBalance();

	

	handleAccount(cacc);

    return 0;
}

