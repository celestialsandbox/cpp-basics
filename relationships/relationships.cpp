// relationships.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "Person.h"

using namespace std;


typedef	struct Element
{
	char x = 5;
	int	 y = 9;
};

typedef	struct Container
{
	Element ee;

	double d;
};



int main()
{
	Element ee = { 7, 8 };
	Container cc;

	cout << sizeof Element << endl;

	cout << sizeof Container << endl;


	Person pp( "Selvyn", { 5, 8, 1965 } );

    return 0;
}

