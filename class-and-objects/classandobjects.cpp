// classandobjects.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>

#include "classesandobjects.h"

using namespace std;

typedef	struct Account
{
	string	accNo;
	double	balance;
};

double	debit(Account& _this, double amt)
{
	return (_this.balance -= amt);
}

double	credit(Account& _this, double amt)
{
	return (_this.balance += amt);
}

bool	isOverDrawn(const Account& _this)
{
	return _this.balance < 0;
}


CreditAccount::CreditAccount(double amt):balance(amt)
{
	cout << "CreditAccount.balance = " << balance << endl;
}

CreditAccount::~CreditAccount()
{
	cout << "Killing CreditAccount.balance = " << balance << endl;
}

double	CreditAccount::debit(double amt)
{
	return (this->balance -= amt);
}

double	CreditAccount::credit(double amt)
{
	return (this->balance += amt);
}

bool	CreditAccount::isOverDrawn() const
{
	return this->balance < 0;
}

double	CreditAccount::getBalance() const
{
	return balance;
}

double	CreditAccount::getBalance()
{
	return balance;
}


void	handleStructures()
{
	// Using a structure...
	Account acc = { "Sels", 45.00 };

	double result = debit(acc, 23);

	result = credit(acc, 45);

	result = debit(acc, 89);

	bool od = isOverDrawn(acc);
}

void	handleClasses()
{
	CreditAccount cacc = { 45 };

	double result = cacc.credit(45);
}

void	handleConstness()
{
	const int x = 6;

	// this line won't work
	//x++;

	const CreditAccount cacc = { 33 };

	cacc.isOverDrawn();

}

int main()
{
	handleStructures();

	handleClasses();

	handleConstness();
	

    return 0;
}

