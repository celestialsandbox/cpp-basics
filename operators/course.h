#pragma once

class Course
{
public:
	static	const int	MAX_CLASS_SIZE = 14;

public:
	Course() {}
	Course(const string& title) :courseTitle(title),count(0) {}
	Course(const Course& other)
		:count(other.count),
		courseTitle(other.courseTitle)
	{
	}

	Course(const Course&& other)
		:count(other.count),
		courseTitle(other.courseTitle)
	{
	}
	Course&	operator=(const Course& other)
	{
		count = other.count;
		courseTitle = other.courseTitle;
	}

	Course(int book) :count(book) {}

	void reserve(int ct);
	int max_size() const
	{
		return 14;
	}

	int	numberBooked() const
	{
		return count;
	}

	Course	operator+(const Course& rh);

	Course & operator++();  // prefix   
	Course operator++(int); // postfix

	 operator string&() {}
private:
	int count = 0; // course bookings
	string courseTitle = "QACPP";
};