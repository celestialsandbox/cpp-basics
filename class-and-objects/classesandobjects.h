#pragma once
#ifndef __CLASSESANDOBJECTS_H
#define	__CLASSESANDOBJECTS_H

#include <iostream>

using std::string;

class CreditAccount
{
private:
	double	balance;
	string  accNo;

public:
	CreditAccount(double amt);
	~CreditAccount();
	double	debit(double amt);
	double	credit(double amt);
	bool	isOverDrawn() const;
	double	getBalance() const;
	double	getBalance();
};
#endif // !__CLASSESANDOBJECTS_H
