// dynamicmemory.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int selsx;

void	fooBar()
{
	static int x = 0;

	cout << "The value of X is: " << x++ << endl;
}

int main()
{
	int y= 4;

	cout << selsx << endl;

	cout << y << endl;
	
	fooBar();
	fooBar();
	fooBar();
	fooBar();

	return 0;
}

