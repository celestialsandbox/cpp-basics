// polymorphism.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Account
{
private:
	string	accNo;
	double	balance;

public:
	Account(const string& no, const double amt)
		:accNo(no), balance(amt)
	{
		cout << "Account::Account(no, amt)" << endl;
	}
public:
	virtual	~Account() 
	{
		cout << "Account::~Account()" << endl;
	}

	double	debit(double amt)
	{
		return balance -= amt;
	}

	double	credit(double amt)
	{
		return balance += amt;
	}

	virtual	double	calculateInterest() = 0;

	string&		getAccNo() { return accNo; }
	virtual	double		getBalance() { return balance; }
};

class CreditAccount : public Account
{
public:
	CreditAccount() :Account("", 0.0)	// just for demo purposes
	{
		cout << "CreditAccount::CreditAccount()" << endl;
	}
	CreditAccount(const string& no, const double amt)
		:Account(no, amt)
	{
		cout << "CreditAccount::CreditAccount(no, amt)" << endl;
	}
	~CreditAccount() 
	{
		cout << "CreditAccount::~CreditAccount()" << endl;
	}

	double	getODLimit() const
	{
		return odLimit;
	}
	double		getBalance() override
	{ return Account::getBalance(); }
	
	double	debit(double amt)
	{
		if (getBalance() - amt >= odLimit)
			Account::debit(amt);
		return getBalance();
	}

	double	calculateInterest() override
	{

	}

private:
	double	odLimit = -200.00;
};

void	handleAccount(Account& acc)
{
	acc.getBalance();
}

void	deleteAccounts(Account *accptr)
{
	delete accptr;
}

int main()
{
	//Account *acc  = new Account("A/30456", 200.00);

	CreditAccount *cacc = new CreditAccount("B/3565", 300.00);

	cacc->getBalance();

	handleAccount(*cacc);


	cacc->debit(400);

	deleteAccounts(cacc);

	return 0;
}

