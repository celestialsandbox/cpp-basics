//#pragma once
#ifndef __OPERATIONS_H
#define	__OPERATIONS_H
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

#define ADD(X, Y)		X + Y
#define MODDIV(F, L)	(F = L % 11)

int			sum(int x, int y);
char		sum(char x, char y);
double		sum(double x, double y);
string		sum(const string& x, const string& y);

void		copied(int& val);


#endif // !__OPERATIONS_H

