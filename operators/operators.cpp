// operators.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

#include "course.h"

using namespace std;

void Course::reserve(int ct)
{
	// sufficient places remaining?
	if (this->count + ct <= max_size())
	{
		count += ct; // update class count?        
	}
}

Course	Course::operator+(const Course& rh)
{
	Course temp;
	if (this->numberBooked() + rh.numberBooked() <= Course::MAX_CLASS_SIZE)
		temp.reserve(this->numberBooked() + rh.numberBooked());
	return temp;
}

Course & Course::operator++()  // prefix   
{
	if (this->numberBooked() + 1 <= Course::MAX_CLASS_SIZE)
		this->reserve(1);
	return *this;
}

Course Course::operator++(int) // postfix
{
	Course temp(this->numberBooked());

	if (this->numberBooked() + 1 <= Course::MAX_CLASS_SIZE)
		this->reserve(1);

	return temp;
}


/*Course	operator+(const Course lh, const Course rh)
{
	Course temp;
	if (lh.numberBooked() + rh.numberBooked() <= Course::MAX_CLASS_SIZE)
		temp.reserve(lh.numberBooked() + rh.numberBooked());
	return temp;
}*/

int main()
{
	Course c1;
	Course c2;

	c1.reserve(2);
	c2.reserve(4);

	Course c3 = c1 + c2;

	c1.reserve(2);
	c2.reserve(8);

	c3 = c1 + c2;

	c3++;

	++c3;

	Course newc("QACPP");

	string ss = newc;

	return 0;
}

